-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2016 at 02:24 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `warehouse`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`id` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `authKey` varchar(50) DEFAULT NULL,
  `accessToken` varchar(50) DEFAULT NULL,
  `role` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `authKey`, `accessToken`, `role`) VALUES
(0, 'hifa', 'hifacantik', 'hifaulala', 'hifasolehah', 'Apoteker'),
(1, 'ifah', 'ifahcantik', 'ifahulala', 'hifasolehah', 'Apoteker'),
(2, 'hastun', 'hastun', 'hastunimut', 'hastunoke', 'KG'),
(3, 'risya', 'risya', 'risyaimut', 'risyaoke', 'KG'),
(4, 'aku', 'aku', 'akucantik', 'akuimut', 'SA'),
(5, 'kamu', 'kamu', 'kamucantik', 'kamuimut', 'SA');

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE IF NOT EXISTS `obat` (
`id` int(10) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `stock` int(6) NOT NULL,
  `harga_beli` int(7) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga_jual` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `obat_jenis`
--

CREATE TABLE IF NOT EXISTS `obat_jenis` (
`id` int(10) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `obat_kadaluarsa`
--

CREATE TABLE IF NOT EXISTS `obat_kadaluarsa` (
  `tanggal_masuk` date NOT NULL,
`id_obat_kadaluarsa` int(10) NOT NULL,
  `jumlah` int(6) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tanggal_kadaluarsa` date NOT NULL,
  `supplier` varchar(20) NOT NULL,
  `id_obat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `obat_keluar`
--

CREATE TABLE IF NOT EXISTS `obat_keluar` (
`id_obat_keluar` int(20) NOT NULL,
  `id_obat` int(20) NOT NULL,
  `tanggal_keluar` date NOT NULL,
  `nama_obat` varchar(20) NOT NULL,
  `jumlah` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `obat_masuk`
--

CREATE TABLE IF NOT EXISTS `obat_masuk` (
`id_obat_masuk` int(20) NOT NULL,
  `id_obat` int(20) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `tanggal_kadaluarsa` date NOT NULL,
  `nama_obat` varchar(20) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `supplier` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `obat_supplier`
--

CREATE TABLE IF NOT EXISTS `obat_supplier` (
`id` int(10) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obat_jenis`
--
ALTER TABLE `obat_jenis`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obat_kadaluarsa`
--
ALTER TABLE `obat_kadaluarsa`
 ADD PRIMARY KEY (`id_obat_kadaluarsa`);

--
-- Indexes for table `obat_keluar`
--
ALTER TABLE `obat_keluar`
 ADD PRIMARY KEY (`id_obat_keluar`);

--
-- Indexes for table `obat_masuk`
--
ALTER TABLE `obat_masuk`
 ADD PRIMARY KEY (`id_obat_masuk`);

--
-- Indexes for table `obat_supplier`
--
ALTER TABLE `obat_supplier`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `obat`
--
ALTER TABLE `obat`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `obat_jenis`
--
ALTER TABLE `obat_jenis`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `obat_kadaluarsa`
--
ALTER TABLE `obat_kadaluarsa`
MODIFY `id_obat_kadaluarsa` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `obat_keluar`
--
ALTER TABLE `obat_keluar`
MODIFY `id_obat_keluar` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `obat_masuk`
--
ALTER TABLE `obat_masuk`
MODIFY `id_obat_masuk` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `obat_supplier`
--
ALTER TABLE `obat_supplier`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
