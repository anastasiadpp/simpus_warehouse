<?php

namespace app\controllers;

use Yii;
use app\models\ObatKadaluarsa;
use app\models\Obat;
use app\models\ObatSupplier;
use app\models\ObatKadaluarsaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\helpers\Html;

class ObatKadaluarsaController extends Controller
{
	public function behaviors()
	{
		return[
		'access'=>[
				'class'=>AccessControl::className(),
				'rules'=>[
				['actions'=>[
							'index',
							'create',
							'update',
							'delete',
							'view'
						],
						'allow'=>true,
						'matchCallback'=>function(){
							return (
								Yii::$app->user->identity->role=='Apoteker'
							);
						}
					],
				['actions'=>[
							'index',
							'create',
							'update',
							'delete',
							'view'
						],
						'allow'=>true,
						'matchCallback'=>function(){
							return (
								Yii::$app->user->identity->role=='KG'
							);
						}
					],
				['actions'=>[
							'index',
							'create',
							'update',
							'delete',
							'view'
						],
						'allow'=>true,
						'matchCallback'=>function(){
							return (
								Yii::$app->user->identity->role=='SA'
							);
						}
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}
    public function actionIndex()
    {
        $searchModel = new ObatKadaluarsaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ObatKadaluarsa model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ObatKadaluarsa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ObatKadaluarsa();
		$modelObat = new Obat();
		$modelSupplier = new ObatSupplier();
		
		$itemsObat = ArrayHelper::map(Obat::find()->all(),'id','nama');
		$itemsSupplier = ArrayHelper::map(ObatSupplier::find()->all(),'id','nama');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
            return $this->redirect(["index"]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'modelObat' => $modelObat,
				'modelSupplier' => $modelSupplier,
                'itemsObat' => $itemsObat,
				'itemsSupplier' => $itemsSupplier,
            ]);
        }
    }
	public function actionGetNama($id) {        
        Yii::$app->response->format = Response::FORMAT_JSON;
        Obat::find()->where('id = :id', [':id' => $id])->one();
    }
    /**
     * Updates an existing ObatKadaluarsa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_obat_kadaluarsa]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ObatKadaluarsa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ObatKadaluarsa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ObatKadaluarsa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ObatKadaluarsa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
		
}
