<?php

namespace app\controllers;

use Yii;
use app\models\Obat;
use app\models\ObatSearch;
use app\models\ObatJenis;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class ObatController extends Controller
{
	public function behaviors()
	{
		return[
		'access'=>[
				'class'=>AccessControl::className(),
				'rules'=>[
				['actions'=>[
							'index',
							'create',
							'update',
							'delete',
							'view'
						],
						'allow'=>true,
						'matchCallback'=>function(){
							return (
								Yii::$app->user->identity->role=='Apoteker'
							);
						}
					],
				['actions'=>[
							'index',
							'create',
							'update',
							'delete',
							'view'
						],
						'allow'=>true,
						'matchCallback'=>function(){
							return (
								Yii::$app->user->identity->role=='KG'
							);
						}
					],
				['actions'=>[
							'index',
							'create',
							'update',
							'delete',
							'view'
						],
						'allow'=>true,
						'matchCallback'=>function(){
							return (
								Yii::$app->user->identity->role=='SA'
							);
						}
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}
    public function actionIndex()
    {
        $searchModel = new ObatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Obat();

        $modelJenis = new ObatJenis();
        
        $itemsJenis = ArrayHelper::map(ObatJenis::find()->all(),'id','nama');
                    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(["index"]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelJenis' => $modelJenis,
                'itemsJenis' => $itemsJenis,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Obat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}