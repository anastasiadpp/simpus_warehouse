<?php

namespace app\controllers;

use Yii;
use app\models\ObatKeluar;
use app\models\Obat;
use yii\helpers\ArrayHelper;
use app\models\ObatKeluarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class ObatKeluarController extends Controller
{
	public function behaviors()
	{
		return[
		'access'=>[
				'class'=>AccessControl::className(),
				'rules'=>[
				['actions'=>[
							'index',
							'create',
							'update',
							'delete',
							'view'
						],
						'allow'=>true,
						'matchCallback'=>function(){
							return (
								Yii::$app->user->identity->role=='Apoteker'
							);
						}
					],
				['actions'=>[
							'index',
							'create',
							'update',
							'delete',
							'view'
						],
						'allow'=>true,
						'matchCallback'=>function(){
							return (
								Yii::$app->user->identity->role=='KG'
							);
						}
					],
				['actions'=>[
							'index',
							'create',
							'update',
							'delete',
							'view'
						],
						'allow'=>true,
						'matchCallback'=>function(){
							return (
								Yii::$app->user->identity->role=='SA'
							);
						}
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}
    public function actionIndex()
    {
        $searchModel = new ObatKeluarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ObatKeluar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ObatKeluar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ObatKeluar();
		$modelObat = new Obat();
		$itemsObat = ArrayHelper::map(Obat::find()->all(),'id','nama');
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_obat_keluar]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'modelObat' => $modelObat,
				'itemsObat' => $itemsObat,
            ]);
        }
    }

    /**
     * Updates an existing ObatKeluar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_obat_keluar]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ObatKeluar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ObatKeluar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ObatKeluar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ObatKeluar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
