<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use app\models\Obat;


/* @var $this yii\web\View */
/* @var $model app\models\ObatKeluar */

$this->title = 'Create Obat Keluar';
$this->params['breadcrumbs'][] = ['label' => 'Obat Keluar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obat-keluar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'tanggal_keluar')->widget(
	DatePicker::className(),[
    'value' => '11/11/2016',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy/mm/dd'
		]
	])	?>
	
    <?= $form->field($model, 'id_obat')->dropDownList($itemsObat, ['prompt'=>'Choose...'])->label('Nama Obat') ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::activeHiddenInput($model, 'nama_obat', ['value' => Obat::findOne(1)->nama]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
