<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ObatSupplier */

$this->title = 'Update Obat Supplier: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Obat Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="obat-supplier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
