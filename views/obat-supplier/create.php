<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ObatSupplier */

$this->title = 'Create Obat Supplier';
$this->params['breadcrumbs'][] = ['label' => 'Obat Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obat-supplier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
