<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ObatKadaluarsa */

$this->title = 'Update Obat Kadaluarsa: ' . $model->id_obat_kadaluarsa;
$this->params['breadcrumbs'][] = ['label' => 'Obat Kadaluarsas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_obat_kadaluarsa, 'url' => ['view', 'id' => $model->id_obat_kadaluarsa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="obat-kadaluarsa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
