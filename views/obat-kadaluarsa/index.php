<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ObatKadaluarsaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Obat Kadaluarsa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obat-kadaluarsa-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tanggal_masuk',
			'tanggal_kadaluarsa',
            //'id_obat_kadaluarsa',
            'nama',
			'jumlah',
            // 'supplier',
            // 'id_obat',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
