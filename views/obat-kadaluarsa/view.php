<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ObatKadaluarsa */

$this->title = $model->id_obat_kadaluarsa;
$this->params['breadcrumbs'][] = ['label' => 'Obat Kadaluarsa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obat-kadaluarsa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_obat_kadaluarsa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_obat_kadaluarsa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tanggal_masuk',
            'id_obat_kadaluarsa',
            'jumlah',
            'nama',
            'tanggal_kadaluarsa',
            'supplier',
            'id_obat',
        ],
    ]) ?>

</div>
