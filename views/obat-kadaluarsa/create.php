<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use app\models\Obat;
use app\models\ObatKadaluarsa;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use app\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\ObatKadaluarsa */
$this->title = 'Create Obat Kadaluarsa';
$this->params['breadcrumbs'][] = ['label' => 'Obat Kadaluarsas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obat-kadaluarsa-create">

    <h1><?= Html::encode($this->title) ?></h1>

<div class="obat-kadaluarsa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'tanggal_masuk')->widget(
	DatePicker::className(),[
    'value' => '11/11/2016',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy/mm/dd'
		]
	])	?>
	<?= $form->field($model, 'tanggal_kadaluarsa')->widget(
	DatePicker::className(),[
    'value' => '11/11/2016',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy/mm/dd'
		]
	])	?>
	<?= $form->field($model, 'id_obat')->dropDownList(ArrayHelper::map(Obat::find()->all(),'id','nama'))->label('Nama Obat')?>
    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?= $form->field($model, 'supplier')->dropDownList($itemsSupplier, ['prompt'=>'Choose...'])->label('Nama Supplier') ?>
	
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::activeHiddenInput($model, 'nama', ['value' => Obat::findOne(1)->nama]); ?>
    </div>

    <?php ActiveForm::end(); ?>
	
</div>

</div>
