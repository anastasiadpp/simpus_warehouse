<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObatKadaluarsaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="obat-kadaluarsa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tanggal_masuk') ?>

    <?= $form->field($model, 'id_obat_kadaluarsa') ?>

    <?= $form->field($model, 'jumlah') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'tanggal_kadaluarsa') ?>

    <?php // echo $form->field($model, 'supplier') ?>

    <?php // echo $form->field($model, 'id_obat') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
