<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use app\models\Obat;


/* @var $this yii\web\View */
/* @var $model app\models\ObatMasuk */

$this->title = 'Create Obat Masuk';
$this->params['breadcrumbs'][] = ['label' => 'Obat Masuks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obat-masuk-create">

    <h1><?= Html::encode($this->title) ?></h1>

<div class="obat-masuk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_obat')->dropDownList($itemsObat, ['prompt'=>'Choose...'])->label('Nama Obat') ?>

    <?= $form->field($model, 'tanggal_masuk')->widget(
	DatePicker::className(),[
    'value' => '11/11/2016',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy/mm/dd'
		]
	])	?>

    <?= $form->field($model, 'tanggal_kadaluarsa')->widget(
	DatePicker::className(),[
    'value' => '11/11/2016',
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy/mm/dd'
		]
	])	?>


    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?= $form->field($model, 'supplier')->dropDownList($itemsSupplier, ['prompt'=>'Choose...'])->label('Nama Supplier') ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::activeHiddenInput($model, 'nama_obat', ['value' => Obat::findOne(1)->nama]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
