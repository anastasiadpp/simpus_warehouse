<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ObatMasukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Obat Masuk';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obat-masuk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_obat_masuk',
            'id_obat',
            'tanggal_masuk',
            'tanggal_kadaluarsa',
            'nama_obat',
            // 'jumlah',
            // 'supplier',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
