<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObatMasukSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="obat-masuk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_obat_masuk') ?>

    <?= $form->field($model, 'id_obat') ?>

    <?= $form->field($model, 'tanggal_masuk') ?>

    <?= $form->field($model, 'tanggal_kadaluarsa') ?>

    <?= $form->field($model, 'nama_obat') ?>

    <?php // echo $form->field($model, 'jumlah') ?>

    <?php // echo $form->field($model, 'supplier') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
