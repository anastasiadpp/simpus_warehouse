<?php

/* @var $this yii\web\View */

$this->title = 'SIM Gudang Obat';
?>
<div class="site-index">

    <div class="jumbotron">
		<div class="GudangObat" >	
			<h1><b>GUDANG OBAT</b></h1>
		</div>
			<h3>Sistem Informasi Manajemen Gudang Obat</h3>
			<hr align="center" color="#7f8c8d"> </hr>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4" align="center">
				<div class="lingkaran"> </div>
                <h2 >Stock Obat</h2>
                <p ><a class="btn btn-default" href="http://localhost/simpus_warehouse/web/index.php?r=obat/index">Read More &raquo;</a></p>
            </div>
            <div class="col-lg-4" align="center">
				<div class="lingkaran" > </div>
                <h2>Data Jenis Obat</h2>
                <p><a class="btn btn-default" href="http://localhost/simpus_warehouse/web/index.php?r=obat-jenis/index">Read More &raquo;</a></p>
            </div>
            <div class="col-lg-4" align="center">
				<div class="lingkaran" > </div>
                <h2>Data Obat Kadaluarsa</h2>
                <p><a class="btn btn-default" href="http://localhost/simpus_warehouse/web/index.php?r=obat-kadaluarsa/index">Read More &raquo;</a></p>
            </div>
			<div class="col-lg-4" align="center">
				<div class="lingkaran" > </div>
                <h2>Data Obat Keluar</h2>
                <p><a class="btn btn-default" href="http://localhost/simpus_warehouse/web/index.php?r=obat-keluar/index">Read More &raquo;</a></p>
            </div>
			<div class="col-lg-4" align="center">
				<div class="lingkaran" > </div>
                <h2>Data Obat Masuk</h2>
                <p><a class="btn btn-default" href="http://localhost/simpus_warehouse/web/index.php?r=obat-masuk/index">Read More &raquo;</a></p>
            </div>
			<div class="col-lg-4" align="center">
				<div class="lingkaran" > </div>
                <h2>Data Obat Supplier</h2>
                <p><a class="btn btn-default" href="http://localhost/simpus_warehouse/web/index.php?r=obat-supplier/index">Read More &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
