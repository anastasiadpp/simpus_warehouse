<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObatMasuk;

/**
 * ObatMasukSearch represents the model behind the search form about `app\models\ObatMasuk`.
 */
class ObatMasukSearch extends ObatMasuk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obat_masuk', 'id_obat', 'jumlah'], 'integer'],
            [['tanggal_masuk', 'tanggal_kadaluarsa', 'nama_obat', 'supplier'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObatMasuk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_obat_masuk' => $this->id_obat_masuk,
            'id_obat' => $this->id_obat,
            'tanggal_masuk' => $this->tanggal_masuk,
            'tanggal_kadaluarsa' => $this->tanggal_kadaluarsa,
            'jumlah' => $this->jumlah,
        ]);

        $query->andFilterWhere(['like', 'nama_obat', $this->nama_obat])
            ->andFilterWhere(['like', 'supplier', $this->supplier]);

        return $dataProvider;
    }
}
