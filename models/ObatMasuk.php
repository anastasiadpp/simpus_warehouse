<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "obat_masuk".
 *
 * @property integer $id_obat_masuk
 * @property integer $id_obat
 * @property string $tanggal_masuk
 * @property string $tanggal_kadaluarsa
 * @property string $nama_obat
 * @property integer $jumlah
 * @property string $supplier
 *
 * @property Obat $idObatMasuk
 */
class ObatMasuk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'obat_masuk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obat', 'tanggal_masuk', 'tanggal_kadaluarsa', 'nama_obat', 'jumlah', 'supplier'], 'required'],
            [['id_obat', 'jumlah'], 'integer'],
            [['tanggal_masuk', 'tanggal_kadaluarsa'], 'safe'],
            [['nama_obat', 'supplier'], 'string', 'max' => 20],
            [['id_obat_masuk'], 'exist', 'skipOnError' => true, 'targetClass' => Obat::className(), 'targetAttribute' => ['id_obat_masuk' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obat_masuk' => 'Id Obat Masuk',
            'id_obat' => 'Id Obat',
            'tanggal_masuk' => 'Tanggal Masuk',
            'tanggal_kadaluarsa' => 'Tanggal Kadaluarsa',
            'nama_obat' => 'Nama Obat',
            'jumlah' => 'Jumlah',
            'supplier' => 'Supplier',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObatMasuk()
    {
        return $this->hasOne(Obat::className(), ['id' => 'id_obat_masuk']);
    }
}
