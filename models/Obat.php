<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "obat".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $jenis
 * @property integer $stock
 * @property integer $harga_beli
 * @property string $satuan
 * @property integer $harga_jual
 *
 * @property ObatJenis $jenis0
 * @property ObatKadaluarsa $obatKadaluarsa
 * @property ObatKeluar $obatKeluar
 * @property ObatMasuk $obatMasuk
 */
class Obat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'obat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'jenis', 'stock', 'harga_beli', 'satuan', 'harga_jual'], 'required'],
            [['jenis', 'stock', 'harga_beli', 'harga_jual'], 'integer'],
            [['nama'], 'string', 'max' => 20],
            [['satuan'], 'string', 'max' => 10],
            [['jenis'], 'exist', 'skipOnError' => true, 'targetClass' => ObatJenis::className(), 'targetAttribute' => ['jenis' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'jenis' => 'Jenis',
            'stock' => 'Stock',
            'harga_beli' => 'Harga Beli',
            'satuan' => 'Satuan',
            'harga_jual' => 'Harga Jual',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenis0()
    {
        return $this->hasOne(ObatJenis::className(), ['id' => 'jenis']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObatKadaluarsa()
    {
        return $this->hasOne(ObatKadaluarsa::className(), ['id_obat_kadaluarsa' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObatKeluar()
    {
        return $this->hasOne(ObatKeluar::className(), ['id_obat_keluar' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObatMasuk()
    {
        return $this->hasOne(ObatMasuk::className(), ['id_obat_masuk' => 'id']);
    }
}
