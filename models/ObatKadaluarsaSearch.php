<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObatKadaluarsa;

/**
 * ObatKadaluarsaSearch represents the model behind the search form about `app\models\ObatKadaluarsa`.
 */
class ObatKadaluarsaSearch extends ObatKadaluarsa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_masuk', 'nama', 'tanggal_kadaluarsa', 'supplier'], 'safe'],
            [['id_obat_kadaluarsa', 'jumlah', 'id_obat'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObatKadaluarsa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tanggal_masuk' => $this->tanggal_masuk,
            'id_obat_kadaluarsa' => $this->id_obat_kadaluarsa,
            'jumlah' => $this->jumlah,
            'tanggal_kadaluarsa' => $this->tanggal_kadaluarsa,
            'id_obat' => $this->id_obat,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'supplier', $this->supplier]);

        return $dataProvider;
    }
}
