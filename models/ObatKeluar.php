<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "obat_keluar".
 *
 * @property integer $id_obat_keluar
 * @property integer $id_obat
 * @property string $tanggal_keluar
 * @property string $nama_obat
 * @property integer $jumlah
 *
 * @property Obat $idObatKeluar
 */
class ObatKeluar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'obat_keluar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_obat', 'tanggal_keluar', 'nama_obat', 'jumlah'], 'required'],
            [['id_obat', 'jumlah'], 'integer'],
            [['tanggal_keluar'], 'safe'],
            [['nama_obat'], 'string', 'max' => 20],
            [['id_obat_keluar'], 'exist', 'skipOnError' => true, 'targetClass' => Obat::className(), 'targetAttribute' => ['id_obat_keluar' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_obat_keluar' => 'Id Obat Keluar',
            'id_obat' => 'Id Obat',
            'tanggal_keluar' => 'Tanggal Keluar',
            'nama_obat' => 'Nama Obat',
            'jumlah' => 'Jumlah',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObatKeluar()
    {
        return $this->hasOne(Obat::className(), ['id' => 'id_obat_keluar']);
    }
}
