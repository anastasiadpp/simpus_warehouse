<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "obat_kadaluarsa".
 *
 * @property string $tanggal_masuk
 * @property integer $id_obat_kadaluarsa
 * @property integer $jumlah
 * @property string $nama
 * @property string $tanggal_kadaluarsa
 * @property string $supplier
 * @property integer $id_obat
 *
 * @property Obat $idObatKadaluarsa
 */
class ObatKadaluarsa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'obat_kadaluarsa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_masuk', 'jumlah', 'nama', 'tanggal_kadaluarsa', 'supplier', 'id_obat'], 'required'],
            [['tanggal_masuk', 'tanggal_kadaluarsa'], 'safe'],
            [['jumlah', 'id_obat'], 'integer'],
            [['nama'], 'string', 'max' => 50],
            [['supplier'], 'string', 'max' => 20],
            [['id_obat_kadaluarsa'], 'exist', 'skipOnError' => true, 'targetClass' => Obat::className(), 'targetAttribute' => ['id_obat_kadaluarsa' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tanggal_masuk' => 'Tanggal Masuk',
            'id_obat_kadaluarsa' => 'Id Obat Kadaluarsa',
            'jumlah' => 'Jumlah',
            'nama' => 'Nama',
            'tanggal_kadaluarsa' => 'Tanggal Kadaluarsa',
            'supplier' => 'Supplier',
            'id_obat' => 'Id Obat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdObatKadaluarsa()
    {
        return $this->hasOne(Obat::className(), ['id' => 'id_obat_kadaluarsa']);
    }
}
